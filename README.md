# Material Design Lite layout for CMS Pheix

## Annotation

This repository contains pages layout for CMS Pheix.

## Demos

[Login page](http://pheix.org/mdl/login.html)

[Dashboard main page](http://pheix.org/mdl/index.html)

## Links

Welcome to CMS Pheix: [http://pheix.org](http://pheix.org)

Quick feedback: [http://pheix.org/feedback.html](http://pheix.org/feedback.html)
